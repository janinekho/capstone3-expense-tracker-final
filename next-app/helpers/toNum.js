export default function toNum(str){
const stringArray = [...str]
const filteredArr = stringArray.filter(element => element !== ",");
    return parseInt(filteredArr.reduce((x,y) => x + y)) 
}