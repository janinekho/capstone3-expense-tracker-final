module.exports = {
    API_URL:process.env.NEXT_PUBLIC_API_URL,
    getAccessToken: () => localStorage.getItem('token'),
    //.then(res => res.json())
    toJSON: (response) => response.json()
    //anonymous fuction
}
